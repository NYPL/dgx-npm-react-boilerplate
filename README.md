NYPL NPM React Boilerplate

Quick steps:

1. Clone this repo
2. Remove the .git folder
3. `git init`
4. Add remote, commit, push, be happy.

To run:

1. `npm start`
2. Check localhost:3000