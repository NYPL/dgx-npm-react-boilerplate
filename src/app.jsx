import React from 'react';
import ReactDOM from 'react-dom';
import nyplComponent from './index.jsx';
 
/* app.jsx
 * Used for local development of React Components
 */
ReactDOM.render(React.createElement(nyplComponent), document.getElementById('component'));
